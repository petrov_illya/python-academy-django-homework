books = [
    {'id': 1,
     'title': 'Clean Code: A Handbook of Agile Software Craftsmanship',
     'released_year': 2015,
     'description': 'Programming is about polishing the craft with years of trial and error. '
                    'I wish there was a way to save yourself from all the hard work by '
                    'learning from the mistakes of other programmers? Fortunately, there is, '
                    'and it is known to the world as the Clean Code: A Handbook of Agile Software '
                    'Craftsmanship book from the legendary Uncle Bob.',
     'author_id': 1
     },
    {'id': 2,
     'title': 'The Clean Coder: A Code of Conduct for Professional Programmers',
     'released_year': 2019,
     'description': 'Compiled by the seasoned software engineer and author Robert C. Martin a.k.a. '
                    'Uncle Bob, The Clean Coder book covers the practices, '
                    'techniques, and tools of true software craftsmanship. '
                    'The book not only tells you how to write clean code but also how to build the attitude '
                    'of a skilled professional programmer.',
     'author_id': 1
     },
    {'id': 3,
     'title': 'Code: The Hidden Language of Computer Hardware and Software',
     'released_year': 2000,
     'description': 'Petzold is one of Microsoft’s Seven '
                    'Windows Pioneers and has been writing about programming since 1984. '
                    'First published in 2000, his book about code itself is a perennial '
                    'favorite in the coding world thanks to its readable explanation of how '
                    'programming and code are built into the fabric of everyday life. '
                    'Petzold explains coding and assembly language for a general audience using '
                    'familiar concepts such as Braille and Morse code. Better still, the book is illustrated, '
                    'helping even those who don’t consider themselves code-savvy to follow along '
                    'through the whole thing.',
     'author_id': 2
     },
    {'id': 4,
     'title': 'Fluent Python',
     'released_year': 2015,
     'description': 'Python’s simplicity lets you become productive quickly, but this often means you aren’t '
                    'using everything it has to offer. With this hands-on guide, you’ll learn how to write '
                    'effective, idiomatic Python code by leveraging its best—and possibly '
                    'most neglected—features. Author Luciano Ramalho takes you through Python’s core '
                    'language features and libraries, and shows you how to make your code shorter, faster, '
                    'and more readable at the same time.',
     'author_id': 3
     },
    {'id': 5,
     'title': 'Fluent Python. 2nd edition',
     'released_year': 2022,
     'description': """Python’s simplicity lets you become productive quickly, 
     but often this means you aren’t using everything it has to offer. With the updated edition 
     of this hands-on guide, you’ll learn how to write effective, modern Python 3 code by leveraging its best ideas.
     Don’t waste time bending Python to fit patterns you learned in other languages.
     Discover and apply idiomatic Python 3 features beyond your past experience. 
     Author Luciano Ramalho guides you through Python’s core language features and libraries 
     and teaches you how to make your code shorter, faster, and more readable.""",
     'author_id': 3
     },
]

authors = [
    {'id': 1,
     'first_name': 'Robert',
     'last_name': 'Martin',
     'age': 55
     },
    {'id': 2,
     'first_name': 'Charles',
     'last_name': 'Petzold',
     'age': 65
     },
    {'id': 3,
     'first_name': 'Luciano',
     'last_name': 'Ramalho',
     'age': 51
     }
]
